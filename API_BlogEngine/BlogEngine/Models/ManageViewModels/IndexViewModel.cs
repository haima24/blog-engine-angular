﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Models.ManageViewModels
{
    public class IndexViewModel
    {
        public string FullName { get; set; }

        public string AvatarUrl { get; set; }

        public string Username { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public string Position { get; set; }

        [Display(Name = "Brief Description")]
        public string BriefDescription { get; set; }

        public string StatusMessage { get; set; }
    }
}
