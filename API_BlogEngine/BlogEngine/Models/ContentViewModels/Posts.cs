﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Models.ContentViewModels
{
    public class Posts
    {
        [Key]
        public int PostId { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Content")]
        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public ApplicationUser User { get; set; }

        public ICollection<ManyPostCategory> Categories { get; } = new List<ManyPostCategory>();

        public ICollection<Comment> ChildComments { get; set; } = new List<Comment>();

    }
}
