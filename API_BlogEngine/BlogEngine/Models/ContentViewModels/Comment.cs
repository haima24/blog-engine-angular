﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Models.ContentViewModels
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public Comment ParentComment { get; set; }

        public ICollection<Comment> ChildComments { get; } = new List<Comment>();

        public ApplicationUser User { get; set; }

        public Posts ParentPost { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

    }
}
