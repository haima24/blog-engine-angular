﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Models.ContentViewModels
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public ICollection<ManyPostCategory> Posts { get; } = new List<ManyPostCategory>();
        public ApplicationUser User { get; set; }
    }
}
