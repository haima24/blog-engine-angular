﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Models.ContentViewModels
{
    public class ManyPostCategory
    {
        [Key]
        public int ManyPostCategoryId { get; set; }

        public int PostId { get; set; }
        public Posts Post { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
