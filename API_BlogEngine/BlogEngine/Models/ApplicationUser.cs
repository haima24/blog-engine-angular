﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogEngine.Models.ContentViewModels;
using Microsoft.AspNetCore.Identity;

namespace BlogEngine.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }

        public string AvatarUrl { get; set; }

        public string Position { get; set; }

        public string BriefDescription { get; set; }
        
        public ICollection<Posts> Posts { get; } = new List<Posts>();

        public ICollection<Category> Categories { get; } = new List<Category>();

    }
}
