using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BlogEngine.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Microsoft.AspNetCore.Mvc
{
    public static class HtmlHelperExtensions
    {
        public static string ToFirstImage(String str,IUrlHelper urlHelper)
        {
            var image = string.Empty;
            string pattern = @"<(img)\b[^>]*>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(str);

            var img = matches.FirstOrDefault();
            if (img != null)
            {
                image = img.Value;
            }
            else {
                var imgPath = urlHelper.Content("~/assets/images/logo.png");
                 image= @"<img src='"+imgPath+"' alt=''>";
            }

            return image;
        }

        public static string RemoveFirstImage(String str)
        {
            string pattern = @"<(img)\b[^>]*>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(str);

            var img = matches.FirstOrDefault();
            if (img != null)
            {
                str = str.Replace(img.Value, string.Empty);
            }

            return str;
        }
    }
}
