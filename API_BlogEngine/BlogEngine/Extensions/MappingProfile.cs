﻿using AutoMapper;
using BlogEngine.Models;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Extensions
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Category, CategoryViewModel>()
                .ForMember(x=>x.IsOwner,x=>x.Ignore())
                .ForMember(x => x.AuthorId, x => x.MapFrom(p => p.User.Id));
            CreateMap<ApplicationUser, AuthorViewModel>()
                .ForMember(x => x.AuthorName, x => x.MapFrom(p=>p.FullName??p.Email))
                .ForMember(x => x.AuthorId, x => x.MapFrom(p => p.Id));
            CreateMap<Posts, PostsViewModel>()
                .ForMember(x => x.ChildCommentCount, x => x.MapFrom(p => p.ChildComments.Count))
                .ForMember(x => x.Categories, x => x.MapFrom(p => p.Categories.Select(m => m.Category)))
                .ForMember(x => x.AuthorId, x => x.MapFrom(p => p.User.Id))
                .ForMember(x => x.AuthorName, x => x.MapFrom(p => p.User.FullName?? p.User.Email)).ReverseMap();
            CreateMap<Comment, CommentViewModel>()
                .ForMember(x => x.AvatarUrl, x => x.MapFrom(p => p.User.AvatarUrl))
                .ForMember(x => x.ParentPostID, x => x.MapFrom(p => p.ParentPost.PostId))
                .ForMember(x => x.UserId, x => x.MapFrom(p => p.User.Id))
                .ForMember(x => x.Content, x => x.MapFrom(p => p.Description))
                .ForMember(x => x.Email, x => x.MapFrom(p => p.User.Email))
                .ForMember(x => x.UserName, x => x.MapFrom(p => p.User.FullName ?? p.User.Email));
        }
    }
}
