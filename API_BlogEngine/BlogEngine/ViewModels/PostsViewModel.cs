﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.ViewModels
{
    public class PostsViewModel
    {
        public int PostId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int ChildCommentCount { get; set; }

        public List<CategoryViewModel> Categories { get; set; }

        public string AuthorId { get; set; }

        public string AuthorName { get; set; }

    }
}
