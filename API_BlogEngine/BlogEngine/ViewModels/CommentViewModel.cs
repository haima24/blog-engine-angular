﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.ViewModels
{
    public class CommentViewModel
    {
        public string UserName{get;set;}

        public string Email { get; set; }

        public string Content { get; set; }

        public int CommentId { get; set; }

        public string UserId { get; set; }

        public int ParentPostID { get; set; }

        public string AvatarUrl { get; set; }

    }
}
