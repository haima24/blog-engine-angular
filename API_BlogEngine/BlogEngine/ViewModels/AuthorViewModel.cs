﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.ViewModels
{
    public class AuthorViewModel
    {
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}
