﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BlogEngine.Data;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;

namespace BlogEngine.Pages.Posts
{
    public class IndexModel : PageModel
    {
        private readonly UserExtendManager _userService;

        public IndexModel(UserExtendManager userService)
        {
            _userService = userService;
        }

        public IList<BlogEngine.Models.ContentViewModels.Posts> Posts { get;set; }

        public async Task OnGetAsync()
        {
            Posts = await _userService.GetPostsOfUser(string.Empty);
        }
    }
}
