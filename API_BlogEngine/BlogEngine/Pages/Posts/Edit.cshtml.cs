﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BlogEngine.Data;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;
using BlogEngine.Controllers;
using System.ComponentModel.DataAnnotations;
using BlogEngine.ViewModels;
using Microsoft.Extensions.Caching.Memory;
using BlogEngine.Constants;

namespace BlogEngine.Pages.Posts
{
    public class EditModel : PageModel
    {
        private readonly IPostService _postService;

        private readonly ICategoryService _categoryService;

        private IMemoryCache _cache;

        private readonly UserExtendManager _userExtendManager;

        public EditModel(IPostService postService, ICategoryService categoryService, UserExtendManager userExtendManager,IMemoryCache cache)
        {
            _postService = postService;
            _userExtendManager = userExtendManager;
            _cache = cache;
            _categoryService = categoryService;
        }

        [BindProperty]
        public BlogEngine.Models.ContentViewModels.Posts Posts { get; set; }

        [BindProperty]
        [Display(Name = "Choose cateogies")]
        public string SelectedCategories { get; set; }

        public List<CategoryViewModel> Categories { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Posts = await _postService.GetPost(id.Value);

            if (_userExtendManager.GetUserId(User) != await _userExtendManager.GetUserIdAsync(Posts.User))
            {
                return RedirectToAction("Login", "Account");
            }

            if (Posts == null)
            {
                return NotFound();
            }
            SelectedCategories = string.Join(',', Posts.Categories.Select(x => x.CategoryId));
            await GetData();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                await GetData();
                return Page();
            }


            try
            {
                await _postService.UpdatePost(new PostsViewModel(), SelectedCategories);
                var categories = await _categoryService.GetCategoriesAsync(CommonConstant.DefaultMaximumLoadedCategory, string.Empty);
                _cache.Set(CacheKeys.Categories, categories);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (! await PostsExists(Posts.PostId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private async Task<bool> PostsExists(int id)
        {
            var post = await _postService.GetPost(id);
            return post != null;
        }

        private async Task GetData()
        {
            var categories = await _userExtendManager.GetSortedCategoriesByOwnerAsync(User);
            Categories = categories.Select(x => new CategoryViewModel { CategoryId = x.CategoryId, CategoryName = x.CategoryName }).ToList();
        }
    }
}
