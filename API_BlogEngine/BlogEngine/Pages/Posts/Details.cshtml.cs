﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BlogEngine.Data;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;
using System.ComponentModel.DataAnnotations;

namespace BlogEngine.Pages.Posts
{
    public class DetailsModel : PageModel
    {
        private readonly IPostService _postService;

        public DetailsModel(IPostService postService)
        {
            _postService = postService;
        }

        public BlogEngine.Models.ContentViewModels.Posts Posts { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Posts = await _postService.GetPost(id.Value);

            if (Posts == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
