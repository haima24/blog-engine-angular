﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Constants
{
    public static class CommonConstant
    {
        public const int DefaultMaximumLoadedCategory = 20;
        public const string LocalizeResourceJsonFile = @"Extensions\Localization\resource.json";
    }
}
