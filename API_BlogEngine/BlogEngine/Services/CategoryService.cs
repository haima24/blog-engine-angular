﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BlogEngine.Data;
using BlogEngine.Models.ContentViewModels;
using Microsoft.EntityFrameworkCore;

namespace BlogEngine.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserExtendManager _userExtendManager;

        public CategoryService(ApplicationDbContext dbContext, UserExtendManager userExtendManager) {
            _dbContext = dbContext;
            _userExtendManager = userExtendManager;
        }

        public async Task<Category> AddCategoryAsync(string categoryName,string userId)
        {
            var user = await _userExtendManager.FindByIdAsync(userId);
            var category = new Category { CategoryName = categoryName,User=user };
            await _dbContext.Categories.AddAsync(category);
            await _dbContext.SaveChangesAsync();
            return category;
        }

        public async Task<Category> UpdateCategoryAsync(int categoryId,string newName)
        {
            var cate = await _dbContext.Categories.FirstOrDefaultAsync(x => x.CategoryId == categoryId);
            if (cate != null) {
                cate.CategoryName = newName;
                _dbContext.Categories.Update(cate);
                _dbContext.Entry(cate).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
            }
            return cate ?? new Category();
        }

        public async Task<List<Category>> GetCategoriesAsync(int takeAmount,string userId)
        {
            var categories = new List<Category>();
            if (!string.IsNullOrEmpty(userId))
            {
                var user =await _userExtendManager.FindByIdAsync(userId);
                await _dbContext.Entry(user).Collection(u => u.Categories).LoadAsync();
                categories = user.Categories.OrderByDescending(x => x.Posts.Count).ToList();
            }
            else {
                categories = (await _dbContext.Categories.Include(x => x.Posts)
                .AsNoTracking().ToListAsync()).OrderByDescending(x => x.Posts.Count).ToList();
            }
            
            if (takeAmount == 0)
            {
                return categories;
            }
            else
            {
                return categories.Take(takeAmount).ToList();
            }
        }

        public async Task<bool> DeleteCategoryAsync(int categoryId)
        {
            var cate = await _dbContext.Categories.FirstOrDefaultAsync(x => x.CategoryId == categoryId);
            var result = 0;
            if (cate != null)
            {
                _dbContext.Remove(cate);
                _dbContext.Entry(cate).State = EntityState.Deleted;
                result=await _dbContext.SaveChangesAsync();
            }
            return result > 0;
        }

    }
}
