﻿using BlogEngine.Models.ContentViewModels;
using BlogEngine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlogEngine.Services
{
    public interface IPostService
    {
        Task<Posts> CreatePost(PostsViewModel post, string userId);

        Task<Posts> UpdatePost(PostsViewModel post, string userId);

        Task<Posts> GetPost(int postId);

        Task<List<Posts>> GetAllPost();

        Task<int> DeletePost(int postId);

        Task<List<Posts>> GetPostsAsync();

        Task<List<Posts>> GetPostsByCategoryIdAsync(int categoryId);
    }
}
