﻿using BlogEngine.Models.ContentViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlogEngine.Services
{
    public interface ICategoryService
    {
        Task<List<Category>> GetCategoriesAsync(int takeAmount,string userId);

        Task<Category> AddCategoryAsync(string categoryName, string userId);

        Task<Category> UpdateCategoryAsync(int categoryId, string newName);

        Task<bool> DeleteCategoryAsync(int categoryId);
    }
}
