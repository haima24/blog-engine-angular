﻿using BlogEngine.Data;
using BlogEngine.Models;
using BlogEngine.Models.ContentViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BlogEngine.Services
{
    public class UserExtendManager: UserManager<ApplicationUser>
    {
        private readonly ApplicationDbContext _dbContext;

        public UserExtendManager(ApplicationDbContext dbContext, IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) 
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Category>> GetSortedCategoriesByOwnerAsync(ClaimsPrincipal principal)
        {
            var user = await GetUserAsync(principal);
            await _dbContext.Entry(user).Collection(u => u.Categories).LoadAsync();
            return user.Categories.ToList();
        }

        public async Task<string> GetFullNameOrEmailAsync(ClaimsPrincipal principal)
        {
            var user = await GetUserAsync(principal);
            var name = user.FullName;
            if (string.IsNullOrEmpty(name)) {
                name = user.Email;
            }
            return name;
        }

        public string GetFullNameOrEmail(ApplicationUser user)
        {
            var name = user.FullName;
            if (string.IsNullOrEmpty(name))
            {
                name = user.Email;
            }
            return name;
        }

        public async Task<string> GetEmail(ClaimsPrincipal user)
        {
            var u = await GetUserAsync(user);
            return u.Email;
        }

        public async Task<List<Posts>> GetPostsOfUser(string userId)
        {
            var user = await FindByIdAsync(userId);
            await _dbContext.Entry(user).Collection(u => u.Posts).LoadAsync();
            return user.Posts.ToList();
        }

        public async Task<int> CountUserPostsAsync(ClaimsPrincipal principal)
        {
            var user = await GetUserAsync(principal);
            await _dbContext.Entry(user).Collection(u => u.Posts).LoadAsync();
            return user.Posts.Count;
        }

        public async Task<List<ApplicationUser>> GetAllAuthorsSorted()
        {
            var users=await _dbContext.Users.Include(x => x.Posts).ToListAsync();
            var usersFiltered = users.Where(x => x.Posts.Count > 0).OrderByDescending(p => p.Posts.Count);
            return usersFiltered.ToList();
        }

        public async Task<IdentityResult> SetFullNameAsync(ApplicationUser user,string name)
        {
            user.FullName = name;
            var result= await UpdateAsync(user);
            return result;
        }

        public async Task<IdentityResult> SetAvatarAsync(ApplicationUser user, string avt)
        {
            user.AvatarUrl = avt;
            var result = await UpdateAsync(user);
            return result;
        }

        public async Task<IdentityResult> SetPositionAsync(ApplicationUser user, string pos)
        {
            user.Position = pos;
            var result = await UpdateAsync(user);
            return result;
        }

        public async Task<IdentityResult> SetDescriptionAsync(ApplicationUser user, string des)
        {
            user.BriefDescription = des;
            var result = await UpdateAsync(user);
            return result;
        }

        public bool IsCurrentUserIsOwner(ClaimsPrincipal claims, string userId)
        {
            return GetUserId(claims) == userId;
        }
    }
}
