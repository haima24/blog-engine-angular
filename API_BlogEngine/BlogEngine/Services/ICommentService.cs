﻿using BlogEngine.Models.ContentViewModels;
using BlogEngine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Services
{
    public interface ICommentService
    {

        Task<Comment> CreateComment(CommentViewModel commentViewModel);

    }
}
