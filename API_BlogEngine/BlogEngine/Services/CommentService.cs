﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogEngine.Data;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace BlogEngine.Services
{
    public class CommentService : ICommentService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserExtendManager _userExtendManager;

        public CommentService(ApplicationDbContext dbContext,UserExtendManager userExtendManager) {
            _dbContext = dbContext;
            _userExtendManager = userExtendManager;
        }

        public async Task<Comment> CreateComment(CommentViewModel commentViewModel)
        {
            var parentComment = await _dbContext.Comments.FirstOrDefaultAsync(x => x.CommentId == commentViewModel.CommentId);
            var user =await _userExtendManager.FindByIdAsync(commentViewModel.UserId);
            var newComment = new Comment();
            if (parentComment != null)
            {
                newComment =await CreateComment(commentViewModel, user);
                parentComment.ChildComments.Add(newComment);
                _dbContext.Comments.Update(parentComment);
                _dbContext.Entry(parentComment).State = EntityState.Modified;
            }
            else {
                newComment = await CreateComment(commentViewModel, user);
                _dbContext.Comments.Add(newComment);
            }
            await _dbContext.SaveChangesAsync();
            return newComment;
        }

        private async Task<Comment> CreateComment(CommentViewModel commentViewModel, Models.ApplicationUser user)
        {
            var post = await _dbContext.Posts.FirstOrDefaultAsync(x => x.PostId == commentViewModel.ParentPostID);
            Comment newComment = new Comment
            {
                CreatedDate = DateTime.Now,
                Description = commentViewModel.Content,
                ParentPost = post
            };
            if (user != null)
            {
                newComment.User = user;
            }
            else
            {
                newComment.Email = commentViewModel.Email;
                newComment.UserName = commentViewModel.UserName;
            }

            return newComment;
        }
    }
}
