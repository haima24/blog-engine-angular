﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BlogEngine.Data;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace BlogEngine.Services
{
    public class PostService : IPostService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserExtendManager _userExtendManager;
        private readonly IMapper _mapper;

        public PostService(ApplicationDbContext dbContext, UserExtendManager userExtendManager,
            IMapper mapper) {
            _dbContext = dbContext;
            _userExtendManager = userExtendManager;
            _mapper = mapper;
        }

        public async Task<Posts> CreatePost(PostsViewModel postModel, string userId)
        {
            var categories = postModel.Categories;
            var user = await _userExtendManager.FindByIdAsync(userId);
            var post = _mapper.Map<Posts>(postModel);
            post.CreatedDate = DateTime.Now;
            post.UpdatedDate = DateTime.Now;
            post.User = user;
            if (categories != null) {
                categories.ForEach(x => {
                    var categoryId = x.CategoryId;
                    var category = new ManyPostCategory
                    {
                        CategoryId = categoryId
                    };
                    post.Categories.Add(category);
                });
            }
            await _dbContext.Posts.AddAsync(post);
            await _dbContext.SaveChangesAsync();
            return post;
        }

        public async Task<int> DeletePost(int postId)
        {
            var post = await _dbContext.Posts.AsNoTracking()
                .FirstOrDefaultAsync(m => m.PostId == postId);

            _dbContext.Posts.Remove(post);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Posts>> GetAllPost()
        {
            return await _dbContext.Posts.Include(x => x.ChildComments)
                .ThenInclude(x => x.ParentComment).OrderByDescending(x => x.CreatedDate).AsNoTracking()
                .OrderByDescending(x=>x.UpdatedDate).ToListAsync();
        }


        public async Task<Posts> GetPost(int postId)
        {
            var post = await _dbContext.Posts
                .Include(x => x.User)
                .Include(x=>x.Categories)
                .ThenInclude(cate=>cate.Category)
                .Include(x => x.ChildComments)
                .ThenInclude(x=>x.ParentComment)
                .Include(x => x.ChildComments)
                .ThenInclude(x=>x.User)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.PostId == postId);
            post.ChildComments = post.ChildComments.Where(m => m.ParentComment == null).ToList();
            return post;
        }

        public async Task<Posts> UpdatePost(PostsViewModel postModel, string userId)
        {
            var categories = postModel.Categories;
            var user = await _userExtendManager.FindByIdAsync(userId);
            var post = _mapper.Map<Posts>(postModel);
            post.UpdatedDate = DateTime.Now;
            post.User = user;
            if (categories != null)
            {
                var manyCategoriesToDelete = _dbContext.ManyPostCategory
                    .Where(x => x.PostId == post.PostId).ToList();
                _dbContext.ManyPostCategory.RemoveRange(manyCategoriesToDelete);
                categories.ForEach(x => {
                    var categoryId = x.CategoryId;
                    var category = new ManyPostCategory
                    {
                        CategoryId = categoryId
                    };
                    post.Categories.Add(category);
                });
            }
            _dbContext.Posts.Update(post);
            _dbContext.Entry(post).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return post;
            
        }

        public async Task<List<Posts>> GetPostsAsync()
        {
            var posts = await _dbContext.Posts
                .Include(x=>x.User)
                .Include(x => x.ChildComments)
                .ThenInclude(x => x.ParentComment)
                .OrderByDescending(x => x.CreatedDate)
                .AsNoTracking()
                .ToListAsync();
            //posts.ForEach(x => x.ChildComments = x.ChildComments.Where(m => m.ParentComment == null).ToList());
            return posts;
        }

        public async Task<List<Posts>> GetPostsByCategoryIdAsync(int categoryId)
        {
            var post = await _dbContext.Posts
                .Include(x => x.ChildComments)
                .ThenInclude(x => x.ParentComment)
                .Where(x => x.Categories.Select(m => m.CategoryId).Contains(categoryId))
                .OrderByDescending(x => x.CreatedDate)
                .AsNoTracking()
                .ToListAsync();
            //post.ForEach(x => x.ChildComments = x.ChildComments.Where(m => m.ParentComment == null).ToList());
            return post;
        }
    }
}
