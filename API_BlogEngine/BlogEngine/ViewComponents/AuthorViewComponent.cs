﻿using BlogEngine.Models;
using BlogEngine.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.ViewComponents
{
    public class AuthorViewComponent : ViewComponent
    {
        private readonly UserExtendManager _userService;

        public AuthorViewComponent(UserExtendManager userService) { _userService = userService; }

        public IViewComponentResult Invoke(ApplicationUser user)
        {
            user.FullName = _userService.GetFullNameOrEmail(user);
            return View(user);
        }
    }
}
