﻿using BlogEngine.Models;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.ViewComponents
{
    public class CommentViewComponent : ViewComponent
    {
        private readonly IPostService _postService;
        private readonly ICommentService _commentService;


        public CommentViewComponent(IPostService postService,ICommentService commentService)
        {
            _postService = postService;
            _commentService = commentService;
        }

        //public async Task<IViewComponentResult> InvokeAsync(int postId,int commentId)
        //{
        //    if (postId>0)
        //    {
        //        if (commentId == 0)
        //        {
        //            var post = await _postService.GetPost(postId);
        //            return View(post);
        //        }
        //        else {
        //            var comment = await _commentService.GetCommentsAsync(commentId);
        //            return View("Self", comment);
        //        }
                
        //    }
        //    else {
        //        var comments = await _commentService.GetChildCommentsAsync(commentId);
        //        return View("Childs", comments);
        //    }
            
        //}
    }
}
