﻿using BlogEngine.Constants;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.ViewComponents
{
    public class DockCategoryViewComponent : ViewComponent
    {
        private readonly ICategoryService _categoryService;

        private IMemoryCache _cache;

        public DockCategoryViewComponent(ICategoryService categoryService, IMemoryCache cache)
        {
            _categoryService = categoryService;
            _cache = cache;
        }

        public async Task<IViewComponentResult> InvokeAsync(int topCategory,bool allowManage)
        {
            var categories = new List<Category>();
            ViewBag.AllowManage = allowManage;
            if (topCategory == 0)
            {
                categories = await _categoryService.GetCategoriesAsync(topCategory,string.Empty);
            }
            else {
                categories = await _cache.GetOrCreateAsync(CacheKeys.Categories, async e =>
                {
                    var cachedCate= await _categoryService.GetCategoriesAsync(topCategory, string.Empty);
                    return cachedCate;
                });
            }

            return View(categories);
        }

    }
}
