﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogEngine.Models;
using BlogEngine.Services;
using BlogEngine.ViewModels;
using BlogEngine.Models.ContentViewModels;
using Microsoft.Extensions.Caching.Memory;
using BlogEngine.Constants;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.IdentityModel.Tokens.Jwt;

namespace BlogEngine.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        private IMemoryCache _cache;

        private readonly IMapper _mapper;


        public CategoryController(ICategoryService categoryService, IMemoryCache cache,IMapper mapper)
        {
            _categoryService = categoryService;
            _cache = cache;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> CreateCategory([FromBody]CategoryViewModel model)
        {
            var userIdClaim = User.FindFirst(JwtRegisteredClaimNames.Sid);
            var category= await _categoryService.AddCategoryAsync(model.CategoryName, userIdClaim.Value);
            var categoryModel = _mapper.Map<CategoryViewModel>(category);
            return Ok(categoryModel);
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> UpdateCategory([FromBody]CategoryViewModel model)
        {
            var category = await _categoryService.UpdateCategoryAsync(model.CategoryId, model.CategoryName);
            var categoryModel = _mapper.Map<CategoryViewModel>(category);
            return Ok(categoryModel);
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> DeleteCategory([FromBody]CategoryViewModel model)
        {
            var result = await _categoryService.DeleteCategoryAsync(model.CategoryId);
            return Ok(result);
        }


        public async Task<IActionResult> GetCategories(int topCategory, string userId)
        {
            var categories = new List<Category>();
            if (topCategory == 0)
            {
                categories = await _categoryService.GetCategoriesAsync(topCategory, userId);
            }
            else
            {
                categories = await _cache.GetOrCreateAsync(CacheKeys.Categories, async e =>
                {
                    var cachedCate = await _categoryService.GetCategoriesAsync(topCategory, userId);
                    return cachedCate;
                });
            }
            var viewModelCategories = _mapper.Map<List<CategoryViewModel>>(categories);
            viewModelCategories.ForEach(v =>
            {
                v.IsOwner = false;
                if (!string.IsNullOrEmpty(userId)) {
                    v.IsOwner = v.AuthorId == userId;
                }
            });
            return Ok(viewModelCategories);
        }
    }
}
