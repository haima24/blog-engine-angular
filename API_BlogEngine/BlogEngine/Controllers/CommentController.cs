﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogEngine.Models;
using BlogEngine.Services;
using BlogEngine.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace BlogEngine.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly IPostService _postService;
        private readonly IMapper _mapper;

        public CommentController(ICommentService commentService,IPostService postService,IMapper mapper)
        {
            _mapper = mapper;
            _commentService = commentService;
            _postService = postService;
        }
        public async Task<IActionResult> GetComments(int postId)
        {
            var post = await _postService.GetPost(postId);
            var comments = post.ChildComments.ToList();
            var commentsViewModel = _mapper.Map<List<CommentViewModel>>(comments);
            return Ok(commentsViewModel);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> PostComment([FromBody]CommentViewModel comment)
        {
            var resultComment = await _commentService.CreateComment(comment);
            var commentModel = _mapper.Map<CommentViewModel>(resultComment);
            return Ok(commentModel);
        }

        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
