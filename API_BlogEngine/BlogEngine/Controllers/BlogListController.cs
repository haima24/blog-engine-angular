﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogEngine.Constants;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;
using BlogEngine.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace BlogEngine.Controllers
{
    public class BlogListController : Controller
    {
        private readonly IPostService _postService;
        private readonly ICategoryService _categoryService;
        private readonly UserExtendManager _userService;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public BlogListController(IPostService postService, ICategoryService categoryService,
            IMapper mapper,IMemoryCache cache, UserExtendManager userService)
        {
            _postService = postService;
            _categoryService = categoryService;
            _userService = userService;
            _mapper = mapper;
            _cache = cache;
        }

        public async Task<IActionResult> Index()
        {
            var posts = await _postService.GetPostsAsync();
            var postsViewModel = _mapper.Map<List<PostsViewModel>>(posts);
            return Ok(postsViewModel);
        }

        public async Task<IActionResult> PostDetail(int postId)
        {
            var post = await _postService.GetPost(postId);
            var postViewModel = _mapper.Map<PostsViewModel>(post);
            return Ok(postViewModel);
        }

        public async Task<IActionResult> ViewByCategory(int id)
        {
            var posts = await _postService.GetPostsByCategoryIdAsync(id);
            var postsViewModel = _mapper.Map<List<PostsViewModel>>(posts);
            return Ok(postsViewModel);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetPostsOfUser()
        {
            var userIdClaim = User.FindFirst(JwtRegisteredClaimNames.Sid);
            var posts = await _userService.GetPostsOfUser(userIdClaim.Value);
            var postsViewModel = _mapper.Map<List<PostsViewModel>>(posts);
            return Ok(postsViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetPostsOfAuthor(string userId)
        {
            var posts = await _userService.GetPostsOfUser(userId);
            var postsViewModel = _mapper.Map<List<PostsViewModel>>(posts);
            return Ok(postsViewModel);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> CreatePost([FromBody]PostsViewModel model)
        {
            try
            {
                var userIdClaim = User.FindFirst(JwtRegisteredClaimNames.Sid);

                await _postService.CreatePost(model, userIdClaim.Value);

                var categories = await _categoryService.GetCategoriesAsync(CommonConstant.DefaultMaximumLoadedCategory, string.Empty);

                _cache.Set(CacheKeys.Categories, categories);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(true);

        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> UpdatePost([FromBody]PostsViewModel model)
        {
            try
            {
                var userIdClaim = User.FindFirst(JwtRegisteredClaimNames.Sid);

                await _postService.UpdatePost(model, userIdClaim.Value);

                var categories = await _categoryService.GetCategoriesAsync(CommonConstant.DefaultMaximumLoadedCategory, string.Empty);

                _cache.Set(CacheKeys.Categories, categories);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(true);

        }
    }
}