﻿
$(function () {
    var eventReply = function (e) {
        e.preventDefault();
        var $btn = $(this);
        var loaded = $btn.data('loaded');
        var container = $btn.siblings('.reply-container');
        if (!loaded) {
            var commentId = $btn.data('comment-id');
            $.ajax({
                type: "POST",
                url: showReply,
                data: { commentId: commentId },
                dataType: 'html',
                success: function (d) {
                    container.html(d);
                    $('.show-reply').one('click', eventReply);
                    $('.replay', container).on('click', postReply);
                },
            });
        }
        container.collapse('toggle');
    };
    var postReply = function (e) {
        if (!$(this).data('clicked') || $(this).data('cancel')) {
            var container = $('#commentContainer').detach();
            $(this).siblings('.post-reply').append(container);
            container.show();
            $(this).data('clicked', true);
            $(this).data('cancel', false);
        } else {
            var container = $('#commentContainer').detach();
            $('#masterCommentContainer').append(container);
            container.hide();
            $(this).data('clicked', false);
        }

    };
    var postComment = function (
        content,
        commentId,
        callback
    ) {
        $.ajax({
            type: "POST",
            url: postCommentUrl,
            data: {
                UserName: userName,
                Email: email,
                Content: content,
                CommentId: commentId,
                UserId: userId,
                ParentPostID: parentPostID
            },
            dataType: 'html',
            success: function (d) {
                if (callback) {
                    callback(d);
                }
            }
        });
    };
    $('.replay').on('click', postReply);

    $('.show-reply').one('click', eventReply);
    $('#commentAdd').on('click', function () {
        var container = $('#commentContainer').detach();
        $('#masterCommentContainer').append(container);
        $(container).show();
        $(this).hide();
        $(this).data('hide', true);
    });
    $('#commentCancel').on('click', function () {
        $(this).closest('.media-body').find('.replay').data('cancel', true);
        var container = $('#commentContainer').detach();
        $('#masterCommentContainer').append(container);
        container.hide();
        if ($('#commentAdd').data('hide')) {
            $('#commentAdd').show();
        }
    });
    $('#commentSubmit').on('click', function () {
        if ($('#commentForm').valid()) {
            if (!userName) {
                userName = $('#name').val();
            }
            if (!email) {
                email = $('#useremail').val();
            }

            var commentId = 0;
            var cItem = $(this).closest('.post-reply');
            if (cItem.length > 0) {
                commentId = cItem.data('comment-id') || 0;
            }
            var mesContainer = $(this).closest('.media');
            postComment(
                $('#usermessage').val(),
                commentId, function (d) {
                    $('.no-sym', mesContainer).append(d);
                    $('#usermessage').val('');
                }
            )



            $('#commentContainer').hide();
            $('#commentAdd').show();
            $('#commentAdd').data('hide', false);
        }
    });

});