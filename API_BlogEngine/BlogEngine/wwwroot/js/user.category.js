﻿$(function () {
    var addNewCategory = function () {
        if ($('#newCategoryForm').valid()) {
            $.ajax({
                type: "POST",
                url: addNewCategoryUrl,
                data: {
                    categoryName: $('#newCategory').val(),
                },
                dataType: 'html',
                success: function (d) {
                    bindingEvents(d);
                },
            });
        }
    };
    var showEditCancelCategory = function ($this) {
        $('.manage-category').show();
        var parent = $this.closest('li');
        var linkCate = parent.find('.link-category');
        var inputCate = parent.find('.absform');
        inputCate.hide();
        linkCate.show();
    };
    var updateCateogoryAction = function ($input) {
        $.ajax({
            type: "POST",
            url: updateCategoryUrl,
            data: {
                categoryId: $input.data('categoryid'),
                categoryName: $input.val()
            },
            dataType: 'html',
            success: function (d) {
                bindingEvents(d);
            },
        });
    };
    var deleteCategoryAction = function (e) {
        $.ajax({
            type: "POST",
            url: deleteCategoryUrl,
            data: {
                categoryId: $(this).data('categoryid')
            },
            dataType: 'html',
            success: function (d) {
                bindingEvents(d);
            },
        });
    };
    var eCancelAddCategory = function () {
        $('#newCategoryForm').hide();
    };
    var eAddCategory = function () {
        $('#newCategoryForm').show();
    };
    var eActionUpdateCategory = function () {
        var form = $(this).closest('.absform');
        if (form.valid()) {
            var $input = $('input.update-category', form);
            updateCateogoryAction($input);
            showEditCancelCategory($(this));
        }
    };
    var eActionCancelUpdateCategory = function () {
        showEditCancelCategory($(this));
    };
    var eEditCategory = function () {
        $('.manage-category').hide();
        var parent = $(this).closest('li');
        var linkCate = parent.find('.link-category');
        var inputCate = parent.find('.absform');
        linkCate.hide();
        inputCate.show();
    };
    var bindingEvents = function (html) {
        if (html) {
            $('#listCategory').empty();
            $('#listCategory').append(html);
        }
        $('#listCategory .btn-add-category').on('click', addNewCategory);
        $('#listCategory .add-category').on('click', eAddCategory);
        $('#listCategory .btn-cancel-add-category').on('click', eCancelAddCategory);
        $('#listCategory .action-update-category').on('click', eActionUpdateCategory);
        $('#listCategory .action-cancel-update-category').on('click', eActionCancelUpdateCategory);
        $('#listCategory .edit-category').on('click', eEditCategory);
        $('#listCategory .delete-category').on('click', deleteCategoryAction);
    }
    bindingEvents();
});