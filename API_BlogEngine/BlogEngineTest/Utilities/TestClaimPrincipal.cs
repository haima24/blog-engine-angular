﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace BlogEngineTest.Utilities
{
    public static class TestClaimPrincipal
    {
        public static ClaimsPrincipal GetClaims() {
            var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, "username"),
                    new Claim(ClaimTypes.NameIdentifier, "userId"),
                    new Claim("name", "akai777"),
                };
            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);
            return claimsPrincipal;
        }
    }
}
