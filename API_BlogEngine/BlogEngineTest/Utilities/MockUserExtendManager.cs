﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BlogEngine.Data;
using BlogEngine.Models;
using BlogEngine.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;

namespace BlogEngineTest.Utilities
{
    public static class MockUserExtendManager
    {
        public static UserExtendManager TestUserManager( ApplicationDbContext db=null, IUserStore<ApplicationUser> store = null) 
        {
            store = store ?? new Mock<IUserStore<ApplicationUser>>().Object;
            var options = new Mock<IOptions<IdentityOptions>>();
            var idOptions = new IdentityOptions();
            idOptions.Lockout.AllowedForNewUsers = false;
            options.Setup(o => o.Value).Returns(idOptions);
            var userValidators = new List<IUserValidator<ApplicationUser>>();
            var validator = new Mock<IUserValidator<ApplicationUser>>();
            userValidators.Add(validator.Object);
            var pwdValidators = new List<PasswordValidator<ApplicationUser>>();
            pwdValidators.Add(new PasswordValidator<ApplicationUser>());
            var userManager = new UserExtendManager(db,store, options.Object, new PasswordHasher<ApplicationUser>(),
                userValidators, pwdValidators, new UpperInvariantLookupNormalizer(),
                new IdentityErrorDescriber(), null,
                new Mock<ILogger<UserManager<ApplicationUser>>>().Object);
            validator.Setup(v => v.ValidateAsync(userManager, It.IsAny<ApplicationUser>()))
                .Returns(Task.FromResult(IdentityResult.Success)).Verifiable();
            return userManager;
        }
    }
}
