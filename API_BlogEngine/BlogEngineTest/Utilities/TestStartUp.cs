﻿using BlogEngine;
using BlogEngine.Data;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;

namespace BlogEngineTest.Utilities
{
    public class TestStartUp : Startup
    {
        public TestStartUp(IConfiguration configuration) :
        base(configuration)
        {
        }

        public override void SetUpDataBase(IServiceCollection services)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(connection));
        }

        public override void SetupTestDbContext(IApplicationBuilder app)
        {
            base.SetupTestDbContext(app);
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
            .CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                dbContext.Database.OpenConnection(); // see Resource #2 link why we do this
                //dbContext.Database.EnsureCreated();
                // run Migrations
                dbContext.Database.Migrate();

            }
        }
    }
}
