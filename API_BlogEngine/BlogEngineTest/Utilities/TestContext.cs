﻿using BlogEngine;
using BlogEngine.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngineTest.Utilities
{

    public class TestContext
    {
        private TestServer _server;
        public HttpClient Client { get; private set; }

        public TestContext()
        {
            SetUpClient();
        }

        private void SetUpClient()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<TestStartUp>()
                .UseContentRoot(@"D:\Projects\DotNet\SD1087-Tu.HuynhMinh\BlogEngine\BlogEngine\BlogEngine")
                .UseEnvironment("Development")
                .UseApplicationInsights()
                .UseConfiguration(new ConfigurationBuilder()
                    .SetBasePath(@"D:\Projects\DotNet\SD1087-Tu.HuynhMinh\BlogEngine\BlogEngine\BlogEngine") // @"C:\Users\usuario\source\repos\CreditCardApp\CreditCardApp"
                    .AddJsonFile("appsettings.json")
                    .Build()
            )
                );
            Client = _server.CreateClient();
        }
       
    }
}
