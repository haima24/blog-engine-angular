using BlogEngine.Data;
using BlogEngine.Models;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;
using BlogEngineTest.Utilities;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading;
using BlogEngine.Controllers;

namespace BlogEngineTest.Tests.Services
{
    public class PostTest
    {
        [Fact]
        public async Task Test_CreatePost()
        {
            using (var context = new ApplicationDbContext(TestDbContextHelper.TestDbContextOptions()))
            {
                var claims = TestClaimPrincipal.GetClaims();
                var userManager = MockUserExtendManager.TestUserManager(context);
                ICategoryService categoryService = new CategoryService(context, userManager);
                var category=await categoryService.AddCategoryAsync("Category1",claims);
                IPostService postService = new PostService(context, userManager);
                var newPost = new Posts { Title = "Post Title 1", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now };
                await postService.CreatePost(newPost, category.CategoryId.ToString(), claims);
                var newPost2= new Posts { Title = "Post Title 2", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now };
                await postService.CreatePost(newPost2, category.CategoryId.ToString(), claims);

                var categoryResult = await postService.GetPostsByCategoryIdAsync(category.CategoryId);

                Assert.IsAssignableFrom<IList<Posts>>(categoryResult);

                Assert.Equal(2, categoryResult.Count);
            }
        }

       
    }
}
