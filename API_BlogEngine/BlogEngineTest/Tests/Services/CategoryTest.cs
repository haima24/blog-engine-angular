using BlogEngine.Data;
using BlogEngine.Models;
using BlogEngine.Models.ContentViewModels;
using BlogEngine.Services;
using BlogEngineTest.Utilities;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading;
using BlogEngine.Controllers;

namespace BlogEngineTest.Tests.Services
{
    public class CategoryTest
    {
        [Fact]
        public async Task Test_CreateCategories()
        {
            using (var context = new ApplicationDbContext(TestDbContextHelper.TestDbContextOptions()))
            {
                var claims = TestClaimPrincipal.GetClaims();
                var userManager = MockUserExtendManager.TestUserManager(context);
                ICategoryService categoryService = new CategoryService(context, userManager);
                await categoryService.AddCategoryAsync("Category1", claims);
                await categoryService.AddCategoryAsync("Category2", claims);
                var categories =await context.Categories.ToListAsync();
                Assert.IsAssignableFrom<IList<Category>>(categories);
                Assert.Equal(2, categories.Count);
            }
        }

        [Fact]
        public async Task Test_UpdateCategory()
        {
            using (var context = new ApplicationDbContext(TestDbContextHelper.TestDbContextOptions()))
            {
                var claims = TestClaimPrincipal.GetClaims();
                var userManager = MockUserExtendManager.TestUserManager(context);
                ICategoryService categoryService = new CategoryService(context, userManager);

                var category = new Category { CategoryName = "TestCategory1" };
                context.Categories.Add(category);
                await context.SaveChangesAsync();

                await categoryService.UpdateCategoryAsync(category.CategoryId, "CategoryNewName");

                var cate=await context.Categories.FirstOrDefaultAsync(f => f.CategoryName == "CategoryNewName");

                Assert.IsAssignableFrom<Category>(cate);
                Assert.Equal("CategoryNewName", cate.CategoryName);
            }
        }
    }
}
