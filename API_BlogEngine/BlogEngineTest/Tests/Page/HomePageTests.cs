﻿using AngleSharp.Dom.Html;
using BlogEngine;
using BlogEngineTest.Utilities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BlogEngineTest.Tests.Page
{
    public class HomePageTests
    {
        private readonly TestContext _sut;

        public HomePageTests()
        {
            _sut = new TestContext();
        }

        [Fact]
        public async Task Get_HomePage()
        {
            var response = await _sut.Client.GetAsync("/");
            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("text/html; charset=utf-8", response.Content.Headers.ContentType.ToString());
        }

        [Theory]
        [InlineData("/")]
        [InlineData("/Home/About")]
        [InlineData("/Home/Contact")]
        [InlineData("/Account/Login")]
        [InlineData("/Account/Register")]
        public async Task CanGetApplicationEndpoints(string url)
        {
            var response = await _sut.Client.GetAsync(url);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("text/html; charset=utf-8", response.Content.Headers.ContentType.ToString());
        }

        [Fact]
        public async Task ManagingUsersRequiresAnAuthenticatedUser()
        {
            var response = await _sut.Client.GetAsync("/Manage/Index");

            // Assert
            Assert.Equal(HttpStatusCode.Redirect, response.StatusCode);
            Assert.StartsWith("http://localhost/Account/Login", response.Headers.Location.ToString());
        }

        [Fact]
        public async Task CanLogin()
        {
            var client = _sut.Client;

            var signUpPage = await client.GetAsync("/Account/Register");
            Assert.Equal(HttpStatusCode.OK, signUpPage.StatusCode);
            var signUpPageHtml = await HtmlHelpers.GetDocumentAsync(signUpPage);

            var signUpSuccess = await client.SendAsync(
                (IHtmlFormElement)signUpPageHtml.QuerySelector("#account"),
                new Dictionary<string, string>
                {
                    ["Email"] = "akai777@gmail.com",
                    ["Password"] = "!QAZ2wsx",
                    ["ConfirmPassword"] = "!QAZ2wsx"
                }
                );
            Assert.Equal(HttpStatusCode.OK, signUpSuccess.StatusCode);

            var loginPage = await client.GetAsync("/Account/Login");
            Assert.Equal(HttpStatusCode.OK, loginPage.StatusCode);
            var loginPageHtml = await HtmlHelpers.GetDocumentAsync(loginPage);

            var profileWithUserName = await client.SendAsync(
                (IHtmlFormElement)loginPageHtml.QuerySelector("#account"),
                new Dictionary<string, string> { ["Email"] = "akai777@gmail.com", ["Password"] = "!QAZ2wsx" });

            Assert.Equal(HttpStatusCode.OK, profileWithUserName.StatusCode);
            var profileWithUserHtml = await HtmlHelpers.GetDocumentAsync(profileWithUserName);
            var userLogin = profileWithUserHtml.QuerySelector("#welcomeLink span");
            Assert.Equal("akai777@gmail.com", userLogin.TextContent);
        }
    }
}
