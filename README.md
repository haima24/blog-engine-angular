# Blog Engine - Angular - Tu Huynh

## Demo Urls

# This web application using techniques:

##	Backend: ASP.NET Core MVC ( ex: localhost:30328 ) 
##	Angular 5 ( SPA ) for building user interfaces ( ex: localhost:4200 )

# This web application can be run by :

##	 download and install .NET Core from here https://www.microsoft.com/net/core

##	From .NET Core Project folder that contain .csproj file (/API_BlogEngine/BlogEngine), open cmd and type dotnet run as below ( server side)
 

###	In project that contain Angular source (/BlogEngine), open cmd and type ng serve ( SPA )
###	For database, using SQL Lite ( BlogEngine.db ) file for this website,
###	All source codes are in gitlab ( public ): https://gitlab.com/haima24/blog-engine-angular.git


- Register: https://localhost:4200/Account/Register

- Login: https://localhost:4200/Account/Login

- View Posts by Category: https://localhost:4200/BlogList/ViewByCategory/:id

- View Posts by Category: https://localhost:4200/BlogList/ViewByCategory/:id

- View Posts details: https://localhost:4200/Posts/Details?id=:id

- Manage Logged In User Profile: https://localhost:4200/Manage/Index

- Manage Posts and Inline Editing Category: https://localhost:4200/Manage/Index

