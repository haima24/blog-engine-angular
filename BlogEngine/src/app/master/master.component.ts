import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['../app.component.css']
})
export class MasterComponent implements OnInit {

  
  loggedIn:boolean=false;
  displayName:string="";

  constructor(private userService:UserService) { }

  ngOnInit() {
    this.userService.checkLoggedIn().subscribe(
      (dt)=>{
        this.loggedIn=true;
        if(this.loggedIn){
          this.displayName=this.userService.getUserNameOrEmail();
        }
      },
      (er)=>{
        this.loggedIn=false;
        localStorage.clear();
      }
    )
    
  }
  logout(){
    if(localStorage.getItem('userToken')){
      localStorage.removeItem('userToken');
      window.location.href="/" //do full redirect to reload master
    }
  }
}
