import { Component, OnInit,Input } from '@angular/core';

import { CategoryService } from '../../services/category-service.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  public categories ;

  model: any = {};

  addingCategory:boolean=false;
  isDuplicateCategoryname:boolean=false;
  updatingCategory:boolean=false;
  
  @Input() ownCategory:boolean;

  constructor(private _cateService: CategoryService) { }

  ngOnInit() {
    this._cateService.getCategories(this.ownCategory).subscribe(data=>{this.categories=data});
  }

  toggleAddingCategory(){
      this.addingCategory=!this.addingCategory;
  }
  toggleUpdatingCategory(cate,isEdit){
    this.categories.forEach(function(obj){  
        obj.updatingCategory=false;
    });
    cate.updatingCategory=isEdit;
}
  saveNewCategory(){
      this._cateService.createCategory(this.model)
      .subscribe(data=>{
        this.categories.push(data);
        this.model.categoryName="";
        this.toggleAddingCategory();
      });
  }
  saveUpdateCategory(cate){
    this._cateService.updateCategory(cate)
      .subscribe(data=>{
        this.toggleUpdatingCategory(cate,false);
      });
  }
  saveDeleteCategory(cate){
    this._cateService.deleteCategory(cate)
      .subscribe(data=>{
          this.categories=this.categories.filter(x=>x.categoryId!=cate.categoryId);
      });
  }
}
