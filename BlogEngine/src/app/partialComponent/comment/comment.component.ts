import { Component, OnInit, Input } from '@angular/core';
import { CommentService } from '../../services/comment.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  public comments ;
  loggedIn:boolean=false;
  displayName:string="";
  avatar:string="";
  model:any={};

  constructor(private _commentService:CommentService,
    private _userService:UserService,
    private route: ActivatedRoute,) { }

  ngOnInit() {
    var id=this.route.snapshot.paramMap.get('id');
    this.model.parentPostId=id;
    this._commentService.getComments(id).subscribe(data=>{
      this.comments=data;
    });

    this._userService.checkLoggedIn().subscribe(
      (dt)=>{
        this.loggedIn=true;
        if(this.loggedIn){
          this.displayName=this._userService.getUserNameOrEmail();
          this.avatar=this._userService.getAvatarUrl();
        }
      },
      (er)=>{
        this.loggedIn=false;
        localStorage.clear();
      }
    )
    
  }
  postComment(){
    this._commentService.postComment(this.model)
      .subscribe(data=>{
        this.comments.push(data);
        this.model.content="";
      });
  }
  clearComment(){
    this.model.content="";
  }
}
