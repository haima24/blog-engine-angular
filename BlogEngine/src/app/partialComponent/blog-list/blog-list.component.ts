import { Component, OnInit,Input } from '@angular/core';
import { BlogService } from '../../services/blog-service.service';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {

  @Input() ownPost:boolean;

  public posts ;

  constructor(private _blogService: BlogService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let categoryId=this.route.snapshot.paramMap.get('categoryid');
      if(categoryId!="0"){
        this._blogService.getBlogPosts(categoryId,this.ownPost)
        .pipe(map(data => {
          return Object.entries(data).map(([type, value]) => 
          {
            var obj=value;
            obj.FirstImage=this.toFirstImage(obj.content);
            obj.RemovedFirstImage=this.removeFirstImage(obj.content);
            return obj;
          }
        );
        }))
        .subscribe(r=>this.posts=r);
      }else{
        let authorId=this.route.snapshot.paramMap.get('authorid');
        this._blogService.getBlogPostsByAuthor(authorId)
        .pipe(map(data => {
          return Object.entries(data).map(([type, value]) => 
          {
            var obj=value;
            obj.FirstImage=this.toFirstImage(obj.content);
            obj.RemovedFirstImage=this.removeFirstImage(obj.content);
            return obj;
          }
        );
        }))
        .subscribe(r=>this.posts=r);
      }
      
    });
    
  }

  toFirstImage(str:string){
      var image = "";
      var pattern = /<(img)\b[^>]*>/;
      var matches = str.match(pattern);
      if(matches && matches[0]){
        image = matches[0];
      }else{
        var imgPath ="assets/images/logo.png";
        image= "<img src='"+imgPath+"' alt=''>";
      }     
      return image;    
  }

  removeFirstImage(str:string){
    var pattern = /<(img)\b[^>]*>/;

    var matches = str.match(pattern);
    if(matches && matches[0]){
      str = str.replace(matches[0], "");
    }
    return str;

  }

}
