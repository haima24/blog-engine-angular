import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-author-menu',
  templateUrl: './author-menu.component.html',
  styleUrls: ['./author-menu.component.css']
})
export class AuthorMenuComponent implements OnInit {

  public authors;

  constructor(private _userService:UserService) { }

  ngOnInit() {
    this._userService.getAllAuthorSorted().subscribe(data=>{this.authors=data});
  }

}
