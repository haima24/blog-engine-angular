import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { QuillModule } from 'ngx-quill'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AppRoutingModule } from './/app-routing.module';

import { MasterComponent } from './master/master.component';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule , FormGroup} from '@angular/forms';

import { CategoryService } from './services/category-service.service';
import { CategoriesComponent } from './partialComponent/categories/categories.component';
import { BlogListComponent } from './partialComponent/blog-list/blog-list.component';
import { BlogService } from './services/blog-service.service';
import { UserService } from './services/user.service';
import { CommentService } from './services/comment.service';
import { BlogDetailComponent } from './pages/blog-detail/blog-detail.component';
import { RegisterComponent } from './pages/register/register.component';
import { ValidateEqualDirective } from './directive/validate-equal.directive';
import { LoginComponent } from './pages/login/login.component';
import { ManagePostComponent } from './pages/manage-post/manage-post.component';
import { OwnPostComponent } from './pages/own-post/own-post.component';
import {AuthGuard} from './guard/auth.guard';
import { ProfileComponent } from './pages/profile/profile.component';
import { CommentComponent } from './partialComponent/comment/comment.component';
import { AuthorMenuComponent } from './partialComponent/author-menu/author-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MasterComponent,
    CategoriesComponent,
    BlogListComponent,
    BlogDetailComponent,
    RegisterComponent,
    ValidateEqualDirective,
    LoginComponent,
    ManagePostComponent,
    OwnPostComponent,
    ProfileComponent,
    CommentComponent,
    AuthorMenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [CategoryService,BlogService,UserService,AuthGuard,CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
