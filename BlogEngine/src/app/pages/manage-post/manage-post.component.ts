import { Component, OnInit } from '@angular/core';
import { QuillModule } from 'ngx-quill'
import { CategoryService } from '../../services/category-service.service';
import { BlogService } from '../../services/blog-service.service';
import {Router,ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-manage-post',
  templateUrl: './manage-post.component.html',
  styleUrls: ['./manage-post.component.css']
})
export class ManagePostComponent implements OnInit {

  model: any = {};
  loading = false;
  public dropdownList;
  dropdownSettings = {};
  id:string="";

  constructor(private _cateService: CategoryService,
  private _blogService:BlogService,
  private _router:Router,private route:ActivatedRoute) { }

  ngOnInit() {

    this.id=this.route.snapshot.paramMap.get('id');

    this._cateService.getCategories(true).subscribe(data=>{
      this.model.dropdownList=data;
      this.model.selectedItems = [];
      if(this.id!=null){
        this._blogService.getPostDetail(this.id)
        .subscribe(r=>{
          let list=this.model.dropdownList;
          this.model=r;
          this.model.dropdownList=list;
          this.model.selectedItems=this.model.categories;
        }
        );
      }
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'categoryId',
      textField: 'categoryName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  save()
  {
    this.loading=true;
    if(this.id){
      this._blogService.updatePost(this.model).subscribe(
        (data)=>{
            if(data){
                this._router.navigate(["/home"])
            }
        },
        (err)=>{
  
        }
      )
    }else{
      this._blogService.createPost(this.model).subscribe(
        (data)=>{
            if(data){
                this._router.navigate(["/home"])
            }
        },
        (err)=>{
  
        }
      )
    }
    
  }
}
