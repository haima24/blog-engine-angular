import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../services/blog-service.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {

 public entry:any={} ;
 isOwner:boolean=false;

  constructor(private _blogService: BlogService,
    private route: ActivatedRoute,
    private userService:UserService) { }

  ngOnInit() {
    var id=this.route.snapshot.paramMap.get('id');
    this._blogService.getPostDetail(id)
    .subscribe(r=>{
      this.entry=r;
      this.isOwner=this.userService.checkOwner(this.entry.authorId);
    }
    );
    
  }

}
