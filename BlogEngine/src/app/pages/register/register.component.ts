import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';  
import { UserService } from '../../services/user.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model: any = {};
  errors:any=[];
  loading = false;

  constructor(private _userService:UserService,private router: Router) { }

  ngOnInit() {
  }
  register()
  {
    this.loading=true;
    this._userService.createUser(this.model.email,this.model.confirmPassword).subscribe(
      (data)=>{
      this.loading=false;
      this.router.navigate(["/login"]);
    },(err)=>{
      this.loading=false;
      this.errors=[];
      if (err.  status === 400) {
        // handle validation error
          var ers=err.error
          for (var fieldName in ers) {
              if (ers.hasOwnProperty(fieldName)) {
                  this.errors.push(ers[fieldName]);
              }
          }
      } else {
          this.errors.push("something went wrong!");
      }
    });
  }
}
