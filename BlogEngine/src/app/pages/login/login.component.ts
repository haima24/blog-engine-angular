import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  errors:any=[];
  loading = false;
  dt:any={};
  constructor(private _userService:UserService,private router:Router) { }

  ngOnInit() {
  }

  login(){
    this.loading=true;
    this._userService.loginUser(this.model.email,this.model.password,this.model.remember).subscribe(
      (data)=>{
      this.dt=data;
      localStorage.setItem('userToken', this.dt.tokenString);
      this.loading=false;
      window.location.href="/" //do full redirect to reload master
    },(err)=>{
      this.loading=false;
      this.errors=[];
      if (err.status === 400) {
        // handle validation error
          var ers=err.error
          for (var fieldName in ers) {
              if (ers.hasOwnProperty(fieldName)) {
                  this.errors.push(ers[fieldName]);
              }
          }
      } else {
          this.errors.push("something went wrong!");
      }
    });
  }
}
