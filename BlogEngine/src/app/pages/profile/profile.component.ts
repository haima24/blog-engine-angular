import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  model: any = {};
  errors:any=[];
  success:any=[];
  loading = false;
  fileToUpload: File = null;

  constructor(private _userService:UserService) { }

  ngOnInit() {
    this._userService.getUserProfile().subscribe(data=>{
        this.model=data;
    });
  }
  save(){
    this.success=[];
    this.errors=[];
    this._userService.saveUserProfile(this.model).subscribe(
      (data)=>{
        this.success.push(data);
    },(err)=>{
      this.loading=false;
      this.errors=[];
      if (err.status === 400) {
        // handle validation error
          var ers=err.error
          for (var fieldName in ers) {
              if (ers.hasOwnProperty(fieldName)) {
                  this.errors.push(ers[fieldName]);
              }
          }
      } else {
          this.errors.push("something went wrong!");
      }
    });
  }

  encodeImageFileAsURL(files: FileList){
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    reader.onloadend = ()=>{
      this.model.avatarUrl=reader.result
    }
    reader.readAsDataURL(this.fileToUpload);
  }
}
