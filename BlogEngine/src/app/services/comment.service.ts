import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import * as decode from 'jwt-decode'; 

@Injectable()
export class CommentService {

  constructor(private http: HttpClient) { }
  getComments(postId) {
    let uri = environment.baseAPIUrl+'Comment/GetComments';
    let paramObj={params:{postId:postId}};
    return this.http.get(uri,paramObj);
  }
  postComment(model:any){
    let uri = environment.baseAPIUrl+'Comment/PostComment';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    let tokenPayload = decode(authToken); 
    model.userId=tokenPayload.sid
    return this.http.post(uri,model,{headers});	
  }
}
