import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import * as decode from 'jwt-decode'; 

@Injectable()
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories(isOwnCategory) {
    let uri = environment.baseAPIUrl+'Category/GetCategories';
    let paramObj={};
    let userId="";
    let topCategory="0";
    if(isOwnCategory){
      let authToken = localStorage.getItem('userToken');
      let tokenPayload = decode(authToken); 
      userId=tokenPayload.sid;
    }
    paramObj={params:{userId:userId,topCategory:topCategory}};
    return this.http.get(uri,paramObj);
  }

  createCategory(model:any){
    let uri = environment.baseAPIUrl+'Category/CreateCategory';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    return this.http.post(uri,model,{headers});	
  }
  updateCategory(model:any){
    let uri = environment.baseAPIUrl+'Category/UpdateCategory';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    return this.http.post(uri,model,{headers});	
  }
  deleteCategory(model:any){
    let uri = environment.baseAPIUrl+'Category/DeleteCategory';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    return this.http.post(uri,model,{headers});	
  }
}
