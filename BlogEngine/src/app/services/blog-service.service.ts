import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import * as decode from 'jwt-decode'; 

@Injectable()
export class BlogService {

  constructor(private http: HttpClient) { }

  getBlogPosts(categoryId:string,isOwnPost:boolean) {
    let uri="";
    let paramObj={};
    if(isOwnPost){
       uri = environment.baseAPIUrl+'BlogList/GetPostsOfUser';
       let authToken = localStorage.getItem('userToken');
       let headers= new HttpHeaders({
          'Authorization': `Bearer ${authToken}`
       });
       paramObj={headers:headers};
    }else{
      if(categoryId==null){
        uri = environment.baseAPIUrl+'BlogList/Index';
      }else
      {
        uri = environment.baseAPIUrl+'BlogList/ViewByCategory';
        paramObj={params:{id:categoryId}}
      }
    }
    
    return this.http.get(uri,paramObj);
  }

  getBlogPostsByAuthor(authorId:string) {
    let uri="";
    let paramObj={};
    uri = environment.baseAPIUrl+'BlogList/GetPostsOfAuthor';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
       'Authorization': `Bearer ${authToken}`
    });
    paramObj={params:{userId:authorId},headers:headers};
    
    return this.http.get(uri,paramObj);
  }

  getPostDetail(postId:string){
    let uri = environment.baseAPIUrl+'BlogList/PostDetail';
    let data = {postId:postId};
    return this.http.get(uri,{params:data});
  }
  
  createPost(model:any){
    let uri = environment.baseAPIUrl+'BlogList/CreatePost';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    model.categories=model.selectedItems;;
    return this.http.post(uri,model,{headers});	
  }

  updatePost(model:any){
    let uri = environment.baseAPIUrl+'BlogList/UpdatePost';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    model.categories=model.selectedItems;;
    return this.http.post(uri,model,{headers});	
  }
}
