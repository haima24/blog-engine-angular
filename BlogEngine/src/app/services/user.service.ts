import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import * as decode from 'jwt-decode'; 

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  createUser(email:string,password:string){
    let uri = environment.baseAPIUrl+'Account/Register';
    let data = {Email:email,Password:password,ConfirmPassword:password} ;
    return this.http.post(uri,data);
  }
  loginUser(email:string,password:string,rememberMe:boolean){
    let uri = environment.baseAPIUrl+'Account/Login';
    let data = {Email:email,Password:password,RememberMe:rememberMe} ;
    return this.http.post(uri,data);
  }
  checkLoggedIn(){
    let uri = environment.baseAPIUrl+'Account/CheckLogin';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    return this.http.get(uri,{headers});
  }
  checkUserLoggedIn(){
    let authToken = localStorage.getItem('userToken');
    return authToken!=null;
  }
  getUserNameOrEmail(){
    let authToken = localStorage.getItem('userToken');
    let tokenPayload = decode(authToken); 
    return tokenPayload.nameid;
  }
  getAvatarUrl(){
    let authToken = localStorage.getItem('userToken');
    let tokenPayload = decode(authToken); 
    return tokenPayload.sub;
  }
  checkOwner(authorId){
    if(this.checkUserLoggedIn()){
      let authToken = localStorage.getItem('userToken');
      let tokenPayload = decode(authToken); 
      return tokenPayload.sid==authorId;
    }else{
      return false;
    }
  }
  getUserProfile(){
    let uri = environment.baseAPIUrl+'Manage/Index';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    return this.http.get(uri,{headers});
  }
  saveUserProfile(model:any){
    let uri = environment.baseAPIUrl+'Manage/Index';
    let authToken = localStorage.getItem('userToken');
    let headers= new HttpHeaders({
      'Authorization': `Bearer ${authToken}`
    });
    model.categories=model.selectedItems;;
    return this.http.post(uri,model,{headers});	
  }
  getAllAuthorSorted(){
    let uri = environment.baseAPIUrl+'Account/GetAuthors';
    return this.http.get(uri);
  }
}
